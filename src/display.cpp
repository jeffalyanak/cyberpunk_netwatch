#include "display.h"
#include <Arduino.h>
\
void initDisplay()
{
	pinMode(SEGMENT_A, OUTPUT);
	pinMode(SEGMENT_B, OUTPUT);
	pinMode(SEGMENT_C, OUTPUT);
	pinMode(SEGMENT_D, OUTPUT);
	pinMode(SEGMENT_E, OUTPUT);
	pinMode(SEGMENT_F, OUTPUT);
	pinMode(SEGMENT_G, OUTPUT);

	pinMode(DIGIT_1_PIN, OUTPUT);
	pinMode(DIGIT_2_PIN, OUTPUT);
	pinMode(DIGIT_3_PIN, OUTPUT);
	pinMode(DIGIT_4_PIN, OUTPUT);
}

//Given a number, turns on those segments
//If number == 10, then turn off all segments
void lightNumber(int numberToDisplay)
{
#define SEGMENT_ON  LOW
#define SEGMENT_OFF HIGH
	switch (numberToDisplay)
	{
		case 0:
			digitalWrite(SEGMENT_A, SEGMENT_ON);
			digitalWrite(SEGMENT_B, SEGMENT_ON);
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			digitalWrite(SEGMENT_F, SEGMENT_ON);
			break;
		case 1:
			digitalWrite(SEGMENT_B, SEGMENT_ON);
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			break;
		case 2:
			digitalWrite(SEGMENT_A, SEGMENT_ON);
			digitalWrite(SEGMENT_B, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 3:
			digitalWrite(SEGMENT_A, SEGMENT_ON);
			digitalWrite(SEGMENT_B, SEGMENT_ON);
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 4:
			digitalWrite(SEGMENT_B, SEGMENT_ON);
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_F, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 5:
			digitalWrite(SEGMENT_A, SEGMENT_ON);
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_F, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 6:
			digitalWrite(SEGMENT_A, SEGMENT_ON);
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			digitalWrite(SEGMENT_F, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 7:
			digitalWrite(SEGMENT_A, SEGMENT_ON);
			digitalWrite(SEGMENT_B, SEGMENT_ON);
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			break;
		case 8:
			digitalWrite(SEGMENT_A, SEGMENT_ON);
			digitalWrite(SEGMENT_B, SEGMENT_ON);
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			digitalWrite(SEGMENT_F, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 9:
			digitalWrite(SEGMENT_A, SEGMENT_ON);
			digitalWrite(SEGMENT_B, SEGMENT_ON);
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_F, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 10:
			digitalWrite(SEGMENT_A, SEGMENT_OFF);
			digitalWrite(SEGMENT_B, SEGMENT_OFF);
			digitalWrite(SEGMENT_C, SEGMENT_OFF);
			digitalWrite(SEGMENT_D, SEGMENT_OFF);
			digitalWrite(SEGMENT_E, SEGMENT_OFF);
			digitalWrite(SEGMENT_F, SEGMENT_OFF);
			digitalWrite(SEGMENT_G, SEGMENT_OFF);
			break;
		case 'A':
			digitalWrite(SEGMENT_A, SEGMENT_ON);
			digitalWrite(SEGMENT_B, SEGMENT_ON);
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			digitalWrite(SEGMENT_F, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 'a':
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 'B':
			digitalWrite(SEGMENT_A, SEGMENT_ON);
			digitalWrite(SEGMENT_B, SEGMENT_ON);
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			digitalWrite(SEGMENT_F, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 'b':
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			digitalWrite(SEGMENT_F, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 'C':
			digitalWrite(SEGMENT_A, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			digitalWrite(SEGMENT_F, SEGMENT_ON);
			break;
		case 'c':
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 'D':
			digitalWrite(SEGMENT_A, SEGMENT_ON);
			digitalWrite(SEGMENT_B, SEGMENT_ON);
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			break;
		case 'd':
			digitalWrite(SEGMENT_B, SEGMENT_ON);
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 'E':
			digitalWrite(SEGMENT_A, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			digitalWrite(SEGMENT_F, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 'e':
			digitalWrite(SEGMENT_A, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			digitalWrite(SEGMENT_F, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 'F':
		case 'f':
			digitalWrite(SEGMENT_A, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			digitalWrite(SEGMENT_F, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 'G':
		case 'g':
			digitalWrite(SEGMENT_A, SEGMENT_ON);
			digitalWrite(SEGMENT_B, SEGMENT_ON);
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_F, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 'H':
		case 'h':
			digitalWrite(SEGMENT_B, SEGMENT_ON);
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			digitalWrite(SEGMENT_F, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 'I':
		case 'i':
			digitalWrite(SEGMENT_B, SEGMENT_ON);
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			break;
		case 'J':
			digitalWrite(SEGMENT_B, SEGMENT_ON);
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			break;
		case 'j':
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			break;
		case 'K':
		case 'k':
			digitalWrite(SEGMENT_A, SEGMENT_ON);
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			digitalWrite(SEGMENT_F, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 'L':
		case 'l':
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			digitalWrite(SEGMENT_F, SEGMENT_ON);
			break;
		case 'M':
		case 'm':
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 'n':
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 'O':
			digitalWrite(SEGMENT_A, SEGMENT_ON);
			digitalWrite(SEGMENT_B, SEGMENT_ON);
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			digitalWrite(SEGMENT_F, SEGMENT_ON);
			break;
		case 'o':
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 'r':
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 'R':
			digitalWrite(SEGMENT_A, SEGMENT_ON);
			digitalWrite(SEGMENT_B, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			digitalWrite(SEGMENT_F, SEGMENT_ON);
			break;
		case 'S':
		case 's':
			digitalWrite(SEGMENT_A, SEGMENT_ON);
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_F, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 'T':
		case 't':
		case 'u':
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			break;
		case 'y':
			digitalWrite(SEGMENT_B, SEGMENT_ON);
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_F, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case 'Z':
			digitalWrite(SEGMENT_A, SEGMENT_ON);
			digitalWrite(SEGMENT_B, SEGMENT_ON);
			digitalWrite(SEGMENT_C, SEGMENT_ON);
			digitalWrite(SEGMENT_D, SEGMENT_ON);
			digitalWrite(SEGMENT_E, SEGMENT_ON);
			digitalWrite(SEGMENT_F, SEGMENT_ON);
			digitalWrite(SEGMENT_G, SEGMENT_ON);
			break;
		case ' ':
			digitalWrite(SEGMENT_A, SEGMENT_OFF);
			digitalWrite(SEGMENT_B, SEGMENT_OFF);
			digitalWrite(SEGMENT_C, SEGMENT_OFF);
			digitalWrite(SEGMENT_D, SEGMENT_OFF);
			digitalWrite(SEGMENT_E, SEGMENT_OFF);
			digitalWrite(SEGMENT_F, SEGMENT_OFF);
			digitalWrite(SEGMENT_G, SEGMENT_OFF);
			break;
	}
}

void displayNum(int toDisplay)
{
	for(int i = 4 ; i > 0 ; i--)
	{
		switch(i) {
		case 1:
			digitalWrite(DIGIT_1_PIN, HIGH);
			break;
		case 2:
			digitalWrite(DIGIT_2_PIN, HIGH);
			break;
		case 3:
			digitalWrite(DIGIT_3_PIN, HIGH);
			break;
		case 4:
			digitalWrite(DIGIT_4_PIN, HIGH);
			break;
		}

		if(i > 1)
		{
			lightNumber(toDisplay % 10);
		}
		else if(i == 1) //Special case on first digit, don't display 02:11, display 2:11
		{
			if( (toDisplay % 10) != 0) lightNumber(toDisplay % 10);
		}

		toDisplay /= 10;

		delayMicroseconds(DIGIT_ENABLE_DELAY);

		//Turn off all segments
		lightNumber(10);

		//Turn off all digits
		digitalWrite(DIGIT_1_PIN, LOW);
		digitalWrite(DIGIT_2_PIN, LOW);
		digitalWrite(DIGIT_3_PIN, LOW);
		digitalWrite(DIGIT_4_PIN, LOW);
	}

}

//Takes a string like "gren" and displays it, left justified
void displayLetters(char *str)
{
	for(int i = 0 ; i < 4 ; i++)
	{
		//Turn on a digit for a short amount of time
		switch(i)
		{
			case 0:
				digitalWrite(DIGIT_1_PIN, HIGH);
				break;
			case 1:
				digitalWrite(DIGIT_2_PIN, HIGH);
				break;
			case 2:
				digitalWrite(DIGIT_3_PIN, HIGH);
				break;
			case 3:
				digitalWrite(DIGIT_4_PIN, HIGH);
				break;
		}

		//Now display this letter
		lightNumber(str[i]); //Turn on the right segments for this letter

		delayMicroseconds(DIGIT_ENABLE_DELAY);

		//Turn off all segments
		lightNumber(10);
		digitalWrite(DIGIT_1_PIN, LOW);
		digitalWrite(DIGIT_2_PIN, LOW);
		digitalWrite(DIGIT_3_PIN, LOW);
		digitalWrite(DIGIT_4_PIN, LOW);
	}
}