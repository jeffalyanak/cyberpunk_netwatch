#include "main.h"

int show_the_time = false;
int set_the_time = false;

long seconds = 1;
int minutes = 0;
int hours = 12;

void setup()
{
	powerOptimizations();

	pinMode(TOP_BUTTON_PIN, INPUT);
	pinMode(BOTTOM_BUTTON_PIN, INPUT);
	digitalWrite(TOP_BUTTON_PIN, HIGH);    // pull down
	digitalWrite(BOTTOM_BUTTON_PIN, HIGH); // pull down

	pinMode(LED_PIN, OUTPUT);

	initDisplay();

	//Setup TIMER2
	TCCR2A = 0x00;
	TCCR2B = (1<<CS22)|(1<<CS21)|(1<<CS20); //Set CLK/1024 or overflow interrupt every 8s
	ASSR = (1<<AS2); //Enable asynchronous operation
	TIMSK2 = (1<<TOIE2); //Enable the timer 2 interrupt

	//Setup external INT0 interrupt
	EICRA = (1<<ISC01); //Interrupt on falling edge
	EIMSK = (1<<INT0); //Enable INT0 interrupt
	PCMSK1 |= B00000100;
	PCICR |= B00000010;

	digitalWrite(LED_PIN, HIGH);
	showString(const_cast<char *>("JEFF"));
	showString(const_cast<char *>(" EOF"));
	showTime();
	digitalWrite(LED_PIN, LOW);

	sei(); //Enable global interrupts
}

void loop() {
	sleep_mode(); //wakes up if the Timer2 buffer overflows or if you hit the button

	if(show_the_time == true)
	{
		//Debounce
		while(digitalRead(TOP_BUTTON_PIN) == LOW) ; //Wait for you to remove your finger
		delay(100);
		while(digitalRead(TOP_BUTTON_PIN) == LOW) ; //Wait for you to remove your finger
		digitalWrite(LED_PIN, HIGH);
		showTime(); //Show the current time for a few seconds
		digitalWrite(LED_PIN, LOW);

		//If you are holding the bottom button, then you must want to adjust the time
		if(digitalRead(BOTTOM_BUTTON_PIN) == LOW) setTime();

		show_the_time = false;
		set_the_time = false;
	}
}

void powerOptimizations()
{
	//To reduce power, setup all pins as inputs with no pullups
	for(int x = 1 ; x < 18 ; x++)
	{
		pinMode(x, INPUT);
		digitalWrite(x, LOW);
	}

	//Power down various bits of hardware to lower power usage  
	set_sleep_mode(SLEEP_MODE_PWR_SAVE);
	sleep_enable();
	ADCSRA &= ~(1<<ADEN); //Disable ADC
	ACSR = (1<<ACD); //Disable the analog comparator
	DIDR0 = 0x3F; //Disable digital input buffers on all ADC0-ADC5 pins
	DIDR1 = (1<<AIN1D)|(1<<AIN0D); //Disable digital input buffer on AIN1/0

	power_twi_disable();
	power_spi_disable();
	power_timer1_disable();
}

// Timer interrupt
SIGNAL(TIMER2_OVF_vect)
{
	seconds += 8;
	updateTime();
}

// Button interrupts
ISR(PCINT1_vect) 
{
	if (digitalRead(BOTTOM_BUTTON_PIN) == LOW)
	{
		set_the_time = true;
	}
	else
	{
		set_the_time = false;
	}
}
SIGNAL(INT0_vect)
{
	show_the_time = true;
}

void updateTime()
{
	minutes += seconds / 60;
	seconds %= 60;
	hours += minutes / 60;
	minutes %= 60;
	while(hours > 23) hours -= 24;
}

int combineTime(int hours, int minutes)
{
	return (hours * 100) + minutes;
}

void showTime()
{
	boolean buttonPreviouslyHit = false;
	
	unsigned long startTime = millis();
	while( (millis() - startTime) < SHOW_TIME_MS)
	{
		displayNum(combineTime(hours, minutes));

		//If you have hit and released the button while the display is on, start the IR off sequence
		if(digitalRead(TOP_BUTTON_PIN) == LOW)
		{
			buttonPreviouslyHit = true;
		}
		else if( (buttonPreviouslyHit == true) && (digitalRead(TOP_BUTTON_PIN) == HIGH) )
		{
			return;
		}
	}
}

void setTime()
{
	unsigned int idleMiliseconds = 0;
	int buttonHold = 0; 

	while(idleMiliseconds < 2000)
	{
		cli();
		updateTime();
		sei();

		for (int x = 0; x < 20; x++)
		{
			if (x < 10)
			{
				digitalWrite(LED_PIN, HIGH);
			}
			else
			{
				digitalWrite(LED_PIN, LOW);
			}
			displayNum(combineTime(hours, minutes));
			delayMicroseconds(DISPLAY_REFRESH_MS);
		}

		//If you're still hitting the button, then increase the time and reset the idleMili timeout variable
		if(digitalRead(TOP_BUTTON_PIN) == LOW)
		{
			idleMiliseconds = 0;

			buttonHold++;
			if(buttonHold < 10)
			{
				minutes++;
			}
			else
			{
				//Advance the minutes faster because you're holding the button for 10 seconds
				//Start advancing on the tens digit. Floor the single minute digit.
				minutes /= 10; //minutes = 46 / 10 = 4
				minutes *= 10; //minutes = 4 * 10 = 40
				minutes += 10;  //minutes = 40 + 10 = 50
			}
		}
		else
		{
			buttonHold = 0;
		}
		idleMiliseconds += 200;
	}
}

void showString(char *str)
{
	long startTime = millis();
	while( (millis() - startTime) < SHOW_STRING_MS)
	{
		displayLetters(str);
		delayMicroseconds(DISPLAY_REFRESH_MS);
	}
}

