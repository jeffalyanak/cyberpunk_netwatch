#ifndef MAIN_H
#define MAIN_H

#include <Arduino.h>
#include <avr/sleep.h>
#include <avr/power.h>

#include "display.h"
#include "cyber_watch.h"

#define SHOW_TIME_MS 2000
#define SHOW_STRING_MS 500

#define DISPLAY_REFRESH_MS 1500

void setup();
void loop();
void powerOptimizations();

void updateTime();
int combineTime(int hours, int minutes);
void showTime();
void setTime();

void showString(char *str);

#endif /* MAIN_H */
